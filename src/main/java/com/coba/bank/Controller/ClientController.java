package com.coba.bank.Controller;

import com.coba.bank.Service.ClientService;
import com.coba.bank.dao.ClientRepository;
import com.coba.bank.hibernate.Client;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientController {

    private ClientRepository clientRepository;



    private ClientService clientService;

    @PostMapping("/save")
    public ResponseEntity<?> saveClient(@RequestBody Client client){
        clientService.saveclient(client);
        return new ResponseEntity(client, HttpStatus.OK);
    }
    @PostMapping("/update")
    public ResponseEntity<?> updateClient(@RequestBody Client client){
        clientService.updateclient(client);
        return new ResponseEntity(client, HttpStatus.OK);
    }


}
