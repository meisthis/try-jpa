package com.coba.bank.Controller;


import com.coba.bank.Service.BankService;
import com.coba.bank.hibernate.Bank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stay")
public class BankController {

    @Autowired
    private BankService bankService;


    @PostMapping("/save")
    public ResponseEntity<?> saveBank(@RequestBody Bank bank){
        bankService.saveBank(bank);
        return new ResponseEntity(bank, HttpStatus.OK);
    }
    @PostMapping("/update")
    public ResponseEntity<?> updateBank(@RequestBody Bank bank){
        bankService.updateBank(bank);
        return new ResponseEntity(bank, HttpStatus.OK);
    }
    @PostMapping("/delete/{id}")
    public ResponseEntity<?> deletBank(@PathVariable  String id){
        bankService.deleteBank(id);
        return new ResponseEntity(id,HttpStatus.OK);
    }



}
