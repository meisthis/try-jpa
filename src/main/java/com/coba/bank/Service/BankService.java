package com.coba.bank.Service;

import com.coba.bank.hibernate.Bank;
import org.springframework.boot.autoconfigure.SpringBootApplication;

public interface BankService {

    void saveBank(Bank bank);
    void updateBank(Bank bank);
    void deleteBank(String id);
}
