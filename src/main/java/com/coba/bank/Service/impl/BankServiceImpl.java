package com.coba.bank.Service.impl;

import com.coba.bank.Service.BankService;
import com.coba.bank.dao.BankRepository;
import com.coba.bank.hibernate.Bank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;


@Service
public class BankServiceImpl implements BankService {
    @Autowired
    private BankRepository bankRepository;


    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    public void saveBank(Bank bank){
        bankRepository.save(bank);
    }

    public void updateBank(Bank bank){
        bankRepository.save(bank);
    }
    public void deleteBank(String id){
        bankRepository.delete(id);
    }

}
