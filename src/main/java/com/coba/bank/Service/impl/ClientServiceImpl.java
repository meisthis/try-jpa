package com.coba.bank.Service.impl;


import com.coba.bank.Service.ClientService;
import com.coba.bank.dao.ClientRepository;
import com.coba.bank.hibernate.Bank;
import com.coba.bank.hibernate.Client;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

public class ClientServiceImpl implements ClientService {

    @Autowired
     private ClientRepository clientRepository;

    @Override
    @Transactional
    public void saveclient(Client client){
        clientRepository.save(client);
    }

    @Override
    @Transactional
    public void updateclient(Client client){
        clientRepository.save(client);
    }

}
