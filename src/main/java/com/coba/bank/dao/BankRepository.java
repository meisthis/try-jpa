package com.coba.bank.dao;

import com.coba.bank.hibernate.Bank;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface BankRepository extends JpaRepository<Bank,String> {
    Bank findByBankName(String Name);
}
